Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
	
   Because in the logger.properties file, the output to the console only had INFO while the log file had ALL so it had more details.
	
2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	
   From the JUnit API, where its telling us a warning that the "@Disabled" statement in't present when it should be.
	
3.  What does Assertions.assertThrows do?

   It pretty much verifies if an assertion is thrown or not, and allow to have multiple tests.
	
4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)

   It is used during deserialization to verify that the sender and receiver of a serialized object have classes for that object that are compatible, if we don't have it we can run into the issue of using a class that isn't compatible with another class
    		
    2.  Why do we need to override constructors?

   In order to create specialized log messages for that specific instance.
    
    3.  Why we did not override other Exception methods?	

   Because this one was the one Exception we needed to send a special message.
    
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

   It's used this way to make sure there is only one logger per class.

6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

   It stands for Markdown and bitbucket uses this to display the contents of the README onto the homepage of the repository.

7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
   It was failing because it : 1) it was trying to access a null object, and 2) it threw a TimerException in the try block ,but wasn't catching a TimerException in the catch block  so in order to fix it, I added another catch block and removed the finally block so it wouldn't always access a null object.

8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

   The issue occurred in the 2nd test and it seemed to have gone from the bottom to the top to try the tests. The failed test (2nd one) had tried to throw a TimerException, but there was nothing to catch it.The sequence should have gone from TimerException in the try block to TimerException in the catch block.

9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

   DONE

10.  Make a printScreen of your eclipse Maven test run, with console

   DONE

11.  What category of Exceptions is TimerException and what is NullPointerException

   NullPointerException is extended from RuntimeException which is unchecked while TimerException is from Exception and is checked

2.  Push the updated/fixed source code to your own repository.

   DONE
